package de.kramhal.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController()
@RequestMapping("hello")
public class HelloWorldController {

    private static int counter = 0;
    
    @RequestMapping(method = GET)
    public String helloWorld(HttpServletRequest request){
        counter++;
        String url = request.getRequestURL().toString();
        return "Hello World for request " + counter + " to " + url;
    }
}
